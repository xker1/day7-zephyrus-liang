O: 
    1. Use Concept Map to summarize what is OO and showcase to others.
    2. Learn HTTP Syntext, method, Response code and Restful style.
    3. Understand the concept of Pair Programming and experience Pair Programming in practice.
    4. Learn the use of SpringBoot, write a simple interface, and test it with Postman.
    5. Learn some useful stream api from teammates and teachers.
R: interesting and challenging
I: 
    1. The Restful style is very helpful to me, it is a very clever design, and it is very easy to understand when writing interfaces.
    2. I experienced Pair Programming while practicing SpringBoot. Through the exchange of ideas between two people, the speed of problem solving was accelerated and the probability of problems was reduced.
D: Try Pair Programming with other teammates to learn or share more knowledge.