package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private final List<Employee> employees = new ArrayList<>();

    public Employee save(Employee employee) {
        employee.setId(generateId());
        employees.add(employee);
        return employee;
    }

    public Long generateId(){
        return employees.stream()
                .max(Comparator.comparing(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    public List<Employee> getEmployeeList() {
        return employees;
    }

    public Employee getEmployeeById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getALlEmployeeByGender(String gender){
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee updateById(Long id, Employee employee) {
        Employee newEmployee = getEmployeeById(id);
        newEmployee.setAge(employee.getAge());
        newEmployee.setSalary(employee.getSalary());
        newEmployee.setCompanyId(employee.getCompanyId());
        return newEmployee;
    }

    public void deleteById(Long id) {
        Employee employee = getEmployeeById(id);
        employees.removeIf(employeeTemp -> employeeTemp.equals(employee));
    }

    public List<Employee> getPage(Integer page,Integer size) {
        return employees.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }

    public List<Employee> getEmployeeByCompanyId(Long id) {
        return employees.stream().filter(employee -> employee.getCompanyId().equals(id)).collect(Collectors.toList());
    }
}
