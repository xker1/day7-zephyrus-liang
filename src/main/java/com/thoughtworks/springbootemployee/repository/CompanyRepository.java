package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.entity.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    private final List<Company> companies = new ArrayList<>();

    public List<Company> getCompanyList() {
        return companies;
    }

    public Company getCompanyById(Long id) {
        return companies.stream().filter(company -> company.getId().equals(id)).findFirst().orElse(null);
    }

    public List<Company> getPage(Integer page, Integer size) {
        return companies.stream().skip((long) (page - 1) * size).limit(size).collect(Collectors.toList());
    }

    public Company save(Company company) {
        company.setId(generateId());
        companies.add(company);
        return company;
    }

    private Long generateId() {
        return companies.stream()
                .max(Comparator.comparing(Company::getId))
                .map(company -> company.getId() + 1)
                .orElse(1L);
    }

    public Company updateById(Long id, Company company) {
        Company companyToBeUpdated = getCompanyById(id);
        companyToBeUpdated.setName(company.getName());
        return companyToBeUpdated;
    }

    public void deleteById(Long id) {
        companies.removeIf(company -> company.getId().equals(id));
    }
}
