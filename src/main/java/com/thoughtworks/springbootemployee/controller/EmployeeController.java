package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.entity.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RequestMapping("/employees")
@RestController
public class EmployeeController {
    @Resource
    private EmployeeRepository employeeRepository;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Employee createEmployee(@RequestBody Employee employee){
        return employeeRepository.save(employee);
    }

    @GetMapping()
    public List<Employee> getEmployeeList(){
        return employeeRepository.getEmployeeList();
    }

    @GetMapping("/{id}")
    public Employee getEmployeeById(@PathVariable Long id){
        return employeeRepository.getEmployeeById(id);
    }

    @GetMapping(params = {"gender"})
    public List<Employee> getALlEmployeeByGender(@RequestParam String gender){
        return employeeRepository.getALlEmployeeByGender(gender);
    }

    @PutMapping("/{id}")
    public Employee updateEmployeeById(@PathVariable Long id, @RequestBody Employee employee) {
        return employeeRepository.updateById(id, employee);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployeeById(@PathVariable Long id) {
        employeeRepository.deleteById(id);
    }

    @GetMapping(params = {"page","size"})
    public List<Employee> getPage(@RequestParam Integer page,@RequestParam Integer size) {
        return employeeRepository.getPage(page, size);
    }
}
